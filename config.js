var url = require('url');
module.exports = function () {
  var config;
  config = {
      app: {
          url: 'http://localhost:3000',
          name: 'MongoPoc',
          port: 3000,
          secretString: 'secretString',
          maxAge: 3*60*60*1000
      },
      log: {
          level: 'info',
          path: __dirname+'/server.log'
      },
      db: {
          mongoUrl: process.env.MONGOHQ_URL || 'mongodb://localhost/mongopoc',
      }, 
   };

  return config;
};

