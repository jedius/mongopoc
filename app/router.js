var html = glob.modules.fs.readFileSync(__dirname + '/public/index.html');
var length = html.length

var get = function(req, res) {
	console.log('get page');
	res.setHeader('Content-Type', 'text/html');
	res.setHeader('Content-Length', length);
	res.end(html);
};

var search = function(req, res) {
	if (!req.body.request) {
		res.send({
			error: 'bad request'
		});
		return;
	}

	//console.log('request: ', req.body.request);

	var query = searchEngine.parse(req.body.request);

	// console.log(JSON.stringify(query));

	searchEngine.populateTree(query, function(err, entity, children, ancestors) {
		if (!err && entity && children && ancestors) {
			res.send({
				data: {
					entity: entity,
					children: children,
					ancestors: ancestors
				}
			})
		} else {
			res.send({
				error: 'bad request'
			})	
		}
	})
};

var create = function(req, res) {
	if (!req.body.info) {
		console.log(req.body);
		res.send({
			error: 'bad request'
		});
		return;
	}

	searchEngine.create(req.body.info, function(err, entity) {
		if(!err && entity) {
			res.send({
				data: {
					entity: entity
				}
			});
		} else {
			console.log(err);
			res.send({
				error: 'bad request'
			});
		}
	})


};

var addParent = function(req, res) {
	if (!req.body.child || !req.body.parent) {
		res.send({
			error: 'bad request'
		});
		return;
	}

	searchEngine.addParent(req.body.child, req.body.parent, function(err, child, parent) {
		if (!err && child && parent) {
			res.send({
				data: {
					child: child,
					parent: parent
				}
			});
		} else {
			res.send({
				error: 'bad request'
			});
		}
	})
};

module.exports = function(app) {
	console.log('router start');
	app.get('/', get);
	app.post('/search', search);
	app.post('/create', create);
	app.post('/addParent', addParent);
};