console.log('server start');
//console.log(process.env.REDISTOGO_URL);

global.glob = {};
glob.modules = {
  http: require('http'),
  url: require('url'),
  fs: require('fs'),
  child_process: require('child_process'),
  util: require('util'),
  mongoose: require('mongoose'),
  express: require('express'),
  winston: require('winston')
 //paypal: require('paypal-recurring')
};

module.exports.start = function (port,cb) {
  var port = port || 3000;
  glob.config = require('../config')();
  //glob.utils = require('./utils');

  global.mongo = require('./mongo');
  global.searchEngine = require('./searchEngine');

  var express = glob.modules.express
  var app = express();  

  app.configure(function() {
      app.use(express.bodyParser());
      app.use(app.router);
      
      app.use(express["static"](__dirname+'/public'));
  });

  require('./setupdb')(app);
  require('./router')(app);

  console.log(glob.config.db.mongoUrl);
  glob.modules.mongoose.connect(glob.config.db.mongoUrl, function(err) {
    if (err) {
      console.log('mongo err:');
      console.log(err);
    }
    glob.modules.http.createServer(app).listen(port, function() {
      console.log('Server start on port ' + port + ' in ' + app.settings.env + ' mode');
      if (cb) { cb() };
    });
  });
};
