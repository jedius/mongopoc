var tree = require('mongoose-tree');

var mongoose = glob.modules.mongoose,
    Schema = mongoose.Schema;

var entity = new Schema({

  version: {
  	type: 'string'
  },
  fields: {
  	type: 'object'
  },
});

entity.plugin(tree);

var entityModel = mongoose.model('entity', entity); 

module.exports = {
	entity: entityModel
}