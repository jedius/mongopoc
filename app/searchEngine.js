var searchEngine = {

  parse: function(_str) {

    if (_str.indexOf('#') !== -1) {
      if (_str.indexOf('/') !== -1) {
        var field = _str.split("/")[0].split("#")[1];
        var text = _str.split("/")[1];
      } else {
        var text = _str.split("#")[1];
      }
    } else {
      if (_str.indexOf('/') !== -1) {
        var text = _str.split("/")[1];
      } else {
        var text = _str;
      }
    }

    var query = {};

    if (field && field !== "") {
      if (text && text !== "") {
        var prop = 'fields.'+field;

        var arr = new Array();

        query['$or'] = [];
        var q1 = {}
        var q2 = {}
        q1[prop] = {'$regex': text},
        q2[prop] = {'$all':[text]}

        query['$or'].push(q1)
        query['$or'].push(q2)
        // query[prop] = {'$or':[{'$regex': text},{'$all':[text]}]};

        console.log(JSON.stringify(query));
        return query;
      }
    } else {
      if (text && text !== "") {
        var fn = function() {
          var text = "{{__text__}}".toLowerCase();
          for (var key in this.fields) {
              if (typeof this.fields[key] == 'string') {
                if (this.fields[key].toLowerCase().indexOf(text) !== -1) {
                  return true;
                }
              } else if (typeof this.fields[key] == 'object') {
                for (var _key in this.fields[key]) {
                  if (this.fields[key][_key].toLowerCase().indexOf(text) !== -1) {
                    return true
                  }
                }
              }
          }
          return false
        }
        fn = fn.toString().replace('{{__text__}}',text)
        query = {
          $where: fn
        }
      } 
    }

    return query;
  },
  populateTree: function(_query, cb) {
    //console.log(mongo.entity);

    mongo.entity.findOne(_query, function(err, entity) {
      //console.log('entity', entity);
      if (!err && entity) {
        entity.getChildren(true, function(err, children) {
          if (!err && children) {
            entity.getAncestors(function(err, ancestors) {
              if (!err && children) {
                cb(null, entity, children, ancestors);
              } else {
                cb(err, null)
              }
            })
          } else {
            cb(err, null)
          }
        })

      } else {
        if (err) {
          console.log(err);
        }
        cb(err, null)
      }
    })
  },
  create: function(_info, cb) {

    var entity = {
      version: 1,
      fields: _info
    }

    mongo.entity.create(entity, function(err, entity) {
      if (!err && entity) {
        cb(null,entity)
      } else {
        cb(err, null)
      }
    })

  },
  addParent: function(_childId, _parentId, cb) {
    console.log(arguments);
    mongo.entity.findById(_childId,function(err,child) {
      if(!err && child) {
        mongo.entity.findById(_parentId, function(err, parent) {
          if(!err && parent) {
            child.parent = parent
            child.save(function(err) {
              if (!err) {
                cb(null, child, parent)
              } else {e
                cb(err, null)
              }
            })
          } else {
            cb(err,null)
          }
        })
      } else {
        cb(err,null)
      }
    })

  }


}
module.exports = searchEngine;