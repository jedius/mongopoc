var e = mongo.entity

function clearDB(cb) {
  // e.remove({}, function(err, resp) {
    // console.log(resp);
    e.findOne({}, function(err, entity) {
      if (!entity && !err) {
        cb();
      }
    });
  // })
};

function createParentFor(_obj, _parent, cb) {
  //console.log('createParentFor ', _obj, '\n', _parent);

  e.create(_parent, function(err, parent) {
    if (err || !parent) {
      cb(err, false);
    } else {
      _obj.parent = parent

      _obj.save(function(err) {
        if (err) {
          cb(err, false);
        } else {
          parent.save(function(err) {
            if (err) {
              cb(err, false);
            } else {
              console.log('createParent ', parent);
              cb(null, parent);
            }
          })

        }
      })
    }
  })
};

module.exports = function(app) {
  var starwars = {
    "version": 1,
    "fields": {
      "title": "Star Wars",
      "genre": ["Action", "Adventure", "Fantasy", "Sci-Fi"],
      "actor": ["Ford, Harrison", "Fisher, Carrie", "Hamill, Mark"]
    }
  };

  var library = {
    "version": 1,
    "fields": {
      "title": "great lib",
      "genre": ["asd"],
      "Library": "HarrisonA45",
      "Year": "2013"
    }
  }

  var mathem = {
    "version": 1,
    "fields": {
      "title": "applied mathematics",
      "genre": ["Book"]
    }
  }

  clearDB(function() {
    e.create(library, function(err, library) {

      if (err || !library) {
        console.log(err, library);
        return;
      } else {
        createParentFor(library, starwars, function(err, starwars) {
          if (err || !starwars) {
            console.log(err, library);
            return;
          } else {
            createParentFor(starwars, mathem, function(err, mathem) {
              if (err || !mathem) {
                console.log(err, library);
                return;
              } else {
                console.log('db ready');
              }
            })
          }
        });
      }
    });
  });
};